#include "bmpcrypt_error.h"

#include <stdio.h>

void print_error(char* message) {
    printf("Error: ");
    printf(message);
    printf("\n");
}

void print_error_by_code(int code) {
    switch (code) {
        case 0:
            break;
        case EALLOC:
            print_error("can't allocate memory");
            break;
        case EOPENF:
            print_error("can't open file");
            break;
        case ECLOSEF:
            print_error("can't close file");
        case EFORMATF:
            print_error("invalid file format");
            break;
        case EBMPF:
            print_error("not supported bmp-file format");
            break;
        case EIMAGESIZE:
            print_error("image size is too small for the message.");
            break;
        case EFILEWRT:
            print_error("file writing error");
        default:
            print_error("unknown");
            break;
    }
}
