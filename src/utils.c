#include "utils.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "common.h"
#include "bmpcrypt_error.h"

void readline(char *str, int count, FILE *source) {
    int len;

    fgets(str, count, source);
    len = strlen(str);
    str[len-1] = '\0';
}

int read_all_lines(char *str, int count, FILE *source) {
    int res, len, buf_len;
    char *buf;

    res = 0;
    len = 0;
    buf = malloc(sizeof(char) * count);

    if (!buf) {
        res = EALLOC;
    }
    else {
        while (fgets(buf, count, source) &&
            (buf_len = strlen(buf)) + len < count) {
            strcpy(str + len, buf);
            len += buf_len;
        }

        free(buf);
    }
    return res;
}

int is_file_exists(char *path) {
    FILE *file;
    int exists;

    exists = 0;
    file = fopen(path, "w+b");

    if (file) {
        fclose(file);
        exists = 1;
    }
    return exists;
}

int is_path_valid(char *path) {
    FILE *file;
    int valid;

    valid = 0;
    file = fopen(path, "r");
    if (file) {
        fclose(file);
        valid = 1;
    }
    return valid;
}
