#include "config.h"

#include <malloc.h>

#include "common.h"
#include "bmpcrypt_error.h"

Config *create_config(int *res) {
    Config *config;

    *res = 0;

    config = malloc(sizeof(Config));
    if (!config) {
        *res = EALLOC;
    }
    else {
        config->image_file_path = malloc(sizeof(char *) * DEFAULT_MAX_STRING_SIZE);
        config->input_file_path = malloc(sizeof(char *) * DEFAULT_MAX_STRING_SIZE);
        config->output_file_path = malloc(sizeof(char *) * DEFAULT_MAX_STRING_SIZE);

        if (!config->image_file_path ||
            !config->input_file_path ||
            !config->output_file_path) {
                *res = EALLOC;
        }
        else {
            config->is_input_text_file_path_presented = 0;
            config->is_output_file_path_presented = 0;
            config->mode = 0;
        }
    }

    return config;
}

void destroy_config(Config *config) {
    if (config) {
        if (config->image_file_path) {
            free(config->image_file_path);
        }
        if (config->input_file_path) {
            free(config->input_file_path);
        }
        if (config->output_file_path) {
            free(config->output_file_path);
        }
        free(config);
    }
}
