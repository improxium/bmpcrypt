#include "decrypt.h"

int decrypt_image_to_text(BmpImage *image, char *output_text)
{
    int x, y,
        mx, my,
        text_pos,
        res;
    unsigned char nr, ng, nb,
        is_end_of_image;

    text_pos = 0;
    mx = image->header.bi_width;
    my = image->header.bi_height;
    is_end_of_image = 0;

    for (y = 0; y < my && !is_end_of_image; y++)
    {
        for (x = 0; x < mx && !is_end_of_image; x++)
        {
            nr = image->bitmap[y][x].red;
            ng = image->bitmap[y][x].green;
            nb = image->bitmap[y][x].blue;
            
            output_text[text_pos] = (nr & 0x03) | ((ng & 0x07) << 2) | ((nb & 0x07) << 5);

            if (output_text[text_pos] == '\0') {
                is_end_of_image = 1;
            }

            text_pos++; 
        }
    }
    res = 0;
    return res;
}