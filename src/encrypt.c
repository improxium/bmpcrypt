#include "encrypt.h"

#include <string.h>

#include "bmpcrypt_error.h"

int encrypt_text_to_image(BmpImage *image, char *text) {
    int x, y,
        mx, my,
        text_pos,
        text_len,
        res;
    unsigned char r, g, b,
                  nr, ng, nb;

    mx = image->header.bi_width;
    my = image->header.bi_height;
    text_pos = 0;
    text_len = strlen(text);

    if (text_len > mx*my) {
        res = EIMAGESIZE;
    }
    else {
        if (text_len < mx*my) {
            text_len++;
        }
        for (y = 0; y < my; y++) {
            for (x = 0; x < mx && text_pos < text_len; x++) {
                r = image->bitmap[y][x].red;
                g = image->bitmap[y][x].green;
                b = image->bitmap[y][x].blue;

                nr = (r & (0xff - 0x03)) | (text[text_pos] & 0x03);
                ng = (g & (0xff - 0x07)) | ((text[text_pos] >> 2) & 0x07);
                nb = (b & (0xff - 0x07)) | ((text[text_pos] >> 5) & 0x07);
                
                image->bitmap[y][x].red = nr;
                image->bitmap[y][x].green = ng;
                image->bitmap[y][x].blue = nb;

                text_pos++;
            }
        }
        res = 0;
    }

    return res;
}
