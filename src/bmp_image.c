#include "bmp_image.h"
#include "bmpcrypt_error.h"

#include <stdio.h>
#include <malloc.h>

int is_valid_image(BmpImageHeader *header, int file_size) {
    int is_valid;
    is_valid = !(header->bf_size != file_size ||
                 header->bf_reserved1 != 0 ||
                 header->bf_reserved2 != 0 ||
                 header->bi_planes != 1 ||
                 (header->bi_size != 40 && header->bi_size != 108 && header->bi_size != 124) ||
                 header->bf_offbits != header->bi_size+14 ||
                 header->bi_width < 1 || header->bi_width > 10000 ||
                 header->bi_height < 1 || header->bi_height > 10000 ||
                 header->bi_bit_count != 24 ||
                 header->bi_compression !=  0
                );

    return is_valid;
}

int read_header(BmpImageHeader *header, FILE *f) {
    int res,
        read_size,
        file_size;

    res = 0;

    read_size = fread(header, sizeof(char), sizeof(BmpImageHeader), f);
    if (read_size != sizeof(BmpImageHeader)) {
        res = EFORMATF;
    }
    else {
        if (header->bf_type != 0x4d42 && header->bf_type != 0x4349 && header->bf_type != 0x5450 ) {
            res = EFORMATF;
        }
        else {
            fseek(f, 0, SEEK_END);
            file_size = ftell(f);
            fseek(f, sizeof(BmpImageHeader), SEEK_SET);

            if (!is_valid_image(header, file_size)) 
            { 
                res = EFORMATF; 
            }
        }
    }

    return res;
}

int read_pixels(BmpImage *image, FILE *f) {
    unsigned char *tmp_buf;
    int res,
        read_size,
        mx, my, mx3,
        x, y,
        offset;


    res = 0;
    mx = image->header.bi_width;
    my = image->header.bi_height;
    mx3 = (3*mx+3) & (-4);
    tmp_buf = malloc(sizeof(unsigned char) * (mx3*my));

    if (tmp_buf == NULL) {
        res = EALLOC;
    }
    else {
        read_size = fread(tmp_buf, 1, mx3*my, f);
        if (read_size != mx3*my) {
            res = EFORMATF;
        }
        else {
            image->bitmap = malloc(sizeof(Pixel *) * my);
            if (image->bitmap == NULL) {
                res = EALLOC;
            }
            else {
                for(y = 0; y < my; y++) {
                    image->bitmap[y] = malloc(sizeof(Pixel) * mx);
                    if (image->bitmap[y] == NULL) {
                        res = EALLOC;
                    }
                    else {
                        for(x = 0; x < mx; x++) {
                            offset = mx3*(my - y - 1) + 3*x;
                            image->bitmap[y][x].blue = tmp_buf[offset];
                            image->bitmap[y][x].green = tmp_buf[offset + 1];
                            image->bitmap[y][x].red = tmp_buf[offset + 2];
                        }
                    }
                }
            }
        }
        free(tmp_buf);
    }
    return res;
}

int load_image_from_file(BmpImage *image, char *file_path) {
    FILE *f;
    BmpImageHeader header;
    int res;

    f = fopen(file_path, "rb");
    if (!f) {
        res = EOPENF;
    } else {
        res = read_header(&header, f);
        if (res == 0) {
            image->header = header;
            res = read_pixels(image, f);
        }
        if (fclose(f) == EOF) {
            res = EFILEWRT;
        }
    }
    return res;
}

int write_header(BmpImageHeader *header, FILE *f) {
    int written_size,
        res;

    res = 0;

	written_size = fwrite(header, 1, sizeof(BmpImageHeader), f);
	if (written_size != sizeof(BmpImageHeader)) {
        res = EFILEWRT;
    }
    return res;
}

int write_pixels(BmpImage *image, FILE *f) {
    int res,
        mx, my, mx3,
        x, y,
        offset,
        written_size;
    unsigned char *tmp_buf;

    res = 0;
    mx = image->header.bi_width;
    my = image->header.bi_height;
    mx3 = (3*mx+3) & (-4);

    tmp_buf = calloc(mx3*my, sizeof(unsigned char));
    if (tmp_buf == NULL) {
        res = EALLOC;
    }
    else {
        for(y = 0; y < my; y++) {
            for(x = 0; x < mx; x++) {
                offset = mx3*(my - y - 1) + 3*x;
                tmp_buf[offset] = image->bitmap[y][x].blue;
                tmp_buf[offset + 1] = image->bitmap[y][x].green;
                tmp_buf[offset + 2] = image->bitmap[y][x].red;
            }
        }

        written_size = fwrite(tmp_buf, 1, mx3*my, f);
        if (written_size != mx3*my) {
            res = EFILEWRT;
        }
        free(tmp_buf);
    }

    return res;
}

int save_image_to_file(BmpImage *image, char *file_path) {
	FILE *f;
    BmpImageHeader header;
    int res;

    f = fopen(file_path, "wb");
	if (!f) {
        res = EOPENF;
    }
    else {
        header = image->header;
        res = write_header(&header, f);
        if (res == 0) {
            res = write_pixels(image, f);
        }

        if (fclose(f) == EOF) {
            res = ECLOSEF;
        }
    }

	return res;
}

void destroy_image(BmpImage *image) {
    int y;

    if (image->bitmap != NULL) {
        for (y = 0; y < image->header.bi_height; y++) {
            if (image->bitmap[y] != NULL) {
                free(image->bitmap[y]);
            }
        }
        free(image->bitmap);
    }
}
