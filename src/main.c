#include "main.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include "bmpcrypt_error.h"
#include "common.h"
#include "config.h"
#include "encrypt.h"
#include "decrypt.h"
#include "bmp_image.h"
#include "utils.h"

char *read_encryption_text(Config *config, int *res) {
    FILE *file;
    char *text;
    
    *res = 0;

    text = malloc(sizeof(char *) * DEFAULT_MAX_STRING_SIZE);

    if (!text) {
        *res = EALLOC;
    }
    else {
        if (config->is_input_text_file_path_presented) {
            file = fopen(config->input_file_path, "rb");

            if (!file) {
                *res = EOPENF;
            }
            else {
                *res = read_all_lines(text, DEFAULT_MAX_STRING_SIZE, file);
                if (fclose(file) == EOF) {
                    *res = ECLOSEF;
                }
            }
        }
        else {
            printf("Enter text for encryption: ");
            readline(text, DEFAULT_MAX_STRING_SIZE, stdin);
        }
    }

    return text;
}

int save_text_to_file(char *text,  char *path) {
    int res;
    FILE *file;

    res = 0;

    file = fopen(path, "w+b");
    if (!file) {
        res = EOPENF;
    }
    else {
        fputs(text, file);
        if (fclose(file) == EOF) {
            res = ECLOSEF;
        }
    }
    return res;
}

int do_encryption(Config *config) {
    BmpImage image;
    int res;
    char *text_for_encryption;

    res = load_image_from_file(&image, config->image_file_path);
    if (res == 0) {
        text_for_encryption = read_encryption_text(config, &res);
        if (res == 0) {
            res = encrypt_text_to_image(&image, text_for_encryption);
            if (res == 0) {
                res = save_image_to_file(&image, config->output_file_path);
            }
            free(text_for_encryption);
        }
        destroy_image(&image);
    }

    return res;
}

int do_decryption(Config *config) {
    BmpImage image;
    int res;
    char output_text[DEFAULT_MAX_STRING_SIZE];

    res = load_image_from_file(&image, config->image_file_path);

    if (res == 0) {
        res = decrypt_image_to_text(&image, output_text);
        if (res == 0) {
            if (config->is_output_file_path_presented) {
                res = save_text_to_file(output_text, config->output_file_path);
            }
            else {
                printf("Encrypted text:\n");
                printf("%s\n", output_text);
            }
        }
        destroy_image(&image);
    }

    return res;
}

void read_image_path(Config *config) {
    int is_correct;

    is_correct = 0;
    while (!is_correct) {
        printf("Enter path to image: ");
        readline(config->image_file_path, DEFAULT_MAX_STRING_SIZE, stdin);
        
        is_correct = is_path_valid(config->image_file_path);
        if (!is_correct) {
            printf("Invalid file path or file doesn't exist. Try again.\n");
        }
    }
}

void read_input_file_path(Config *config) {
    int is_correct;

    is_correct = 0;
    while (!is_correct) {
        printf("Enter input file path: ");
        readline(config->input_file_path, DEFAULT_MAX_STRING_SIZE, stdin);
        
        is_correct = is_path_valid(config->input_file_path);
        if (!is_correct) {
            printf("Invalid file path or file doesn't exist. Try again.\n");
        }
    }
    config->is_input_text_file_path_presented = 1;
}

int read_input_source() {
    int source,
        is_correct;

    is_correct = 0;
    while (!is_correct) {
        printf("Choose input source:\n");
        printf("1. Stdin\n");
        printf("2. File\n");
        printf("[1/2]> ");
        scanf("%i", &source);
        getchar();
        
        is_correct = source == 1 || source == 2;
        if (!is_correct) {
            printf("Invalid value. Try again.\n");
        }
    }

    return source;
}

void read_output_file_path(Config *config) {
    int is_correct;

    is_correct = 0;
    while (!is_correct) {
        printf("Enter output file path: ");
        readline(config->output_file_path, DEFAULT_MAX_STRING_SIZE, stdin);
        
        is_correct = is_file_exists(config->output_file_path);
        if (!is_correct) {
            printf("Invalid file path. Try again.\n");
        }
    }
    config->is_output_file_path_presented = 1;
}

int read_output_source() {
    int source,
        is_correct;

    is_correct = 0;
    while (!is_correct) {
        printf("Choose output source:\n");
        printf("1. Stdout\n");
        printf("2. File\n");
        printf("[1/2]> ");
        scanf("%i", &source);
        getchar();
        
        is_correct = source == 1 || source == 2;
        if (!is_correct) {
            printf("Invalid value. Try again.\n");
        }
    }

    return source;
}

void read_configuration_for_encryption(Config *config) {
    int input_source;

    read_image_path(config);

    input_source = read_input_source();
    if (input_source == 1) {
        config->is_input_text_file_path_presented = 0;
    }
    else {
        read_input_file_path(config);
    }

    read_output_file_path(config);
}

void read_configuration_for_decryption(Config *config) {
    int output_source;

    read_image_path(config);

    output_source = read_output_source();
    if (output_source == 1) {
        config->is_output_file_path_presented = 0;
    }
    else {
        read_output_file_path(config);
    }
}

void read_mode(Config *config) {
    int is_correct = 0;

    while (!is_correct) {
        printf("Choose mode:\n");
        printf("1. Encryption\n");
        printf("2. Decryption\n");
        printf("3. Version\n");
        printf("4. Exit\n");
        printf("[1/2/3/4]> ");
        scanf("%d", &config->mode);
        getchar();

        is_correct = config->mode == 1 || config->mode == 2 || config->mode == 3 || config->mode == 4;
        if (!is_correct) {
            printf("Invalid input. Try again.\n");
        }
    }

}

void print_version() {
    printf("bmpcrypt-cli v1.0.0\n");
}

int main() {
    while (1) {
        int res;
        Config *config;

        config = create_config(&res);
        if (res != 0) {
            print_error_by_code(res);
        }
        else {
            read_mode(config);

            if (config->mode == 1) {
                read_configuration_for_encryption(config);

                printf("Run encryption...\n");

                res = do_encryption(config);
                if (res == 0) {
                    printf("Done.\n");
                }
                else {
                    print_error_by_code(res);
                }
            }
            else if (config->mode == 2) {
                read_configuration_for_decryption(config);

                printf("Run decryption...\n");

                res = do_decryption(config);
                if (res == 0) {
                    printf("Done.\n");
                }
                else {
                    print_error_by_code(res);
                }
            }
            else if (config->mode == 3) {
                print_version();
            }
            else if (config->mode == 4) {
                exit(0);
            }
            else {
                print_error("invalid mode");
            }

            destroy_config(config);
        }

        puts("");
    }
}
