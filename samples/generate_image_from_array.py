from PIL import Image

print("File name: ", end='')
filename = input()
print("Image size: ", end='')
height, width = map(int, input().split())

bitmap = []
print("Enter bitmap array")
for i in range(0, height):
    print("Row {}: ".format(i), end='')
    bitmap += [list(map(lambda x: (int(x[1:3], base=16), int(x[3:5], base=16), int(x[5:7], base=16)),
                  input().split()))]

print('Generation...')

img = Image.new('RGB', (height, width), 'white')
pixels = img.load()

for i in range(img.size[0]):
    for j in range(img.size[1]):
        pixels[i, j] = bitmap[i][j]

img.save('{}.bmp'.format(filename))

print('Done.')