$PROJECT_DIR = $env:PROJECT_DIR
$BUILD_CONFIGURATION = $env:BUILD_CONFIGURATION

New-Item -ItemType Directory -Force -Path "$PROJECT_DIR/bin" | Out-Null

if (!$BUILD_CONFIGURATION) {
    $BUILD_CONFIGURATION = 'DEBUG'
}

$SOURCE = [array]@(Get-ChildItem $PROJECT_DIR/src/*.c)

Write-Host "Project folder:" $PROJECT_DIR
Write-Host "Build configuration:" $BUILD_CONFIGURATION
Write-Host "Building..."

gcc -g -Wall -o "$PROJECT_DIR/bin/bmpcrypt-cli.exe" -D $BUILD_CONFIGURATION -D 'WINDOWS' -I $PROJECT_DIR/include -I $PROJECT_DIR/src $SOURCE

Write-Host "Done."
