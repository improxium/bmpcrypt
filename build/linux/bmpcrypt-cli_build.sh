#!/bin/bash

if [[ -z "${PROJECT_DIR}" ]]; then
  exit 1
fi

mkdir "${PROJECT_DIR}"

if [[ -z "${BUILD_CONFIGURATION}" ]]; then
  BUILD_CONFIGURATION="DEBUG"
fi

echo "Project folder: ${PROJECT_DIR}"
echo "Build configuration: ${BUILD_CONFIGURATION}"
echo "Building..."

gcc -g -Wall -o "${PROJECT_DIR}/bin/bmpcrypt-cli" -D "${BUILD_CONFIGURATION}" -D "LINUX" -I "${PROJECT_DIR}/include/" -I "${PROJECT_DIR}/src/" `ls ${PROJECT_DIR}/src/*.c`

echo "Done"
