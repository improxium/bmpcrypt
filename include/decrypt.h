#ifndef DECRYPT_H
#define DECRYPT_H

#include "bmp_image.h"

int decrypt_image_to_text(BmpImage *image, char *output_text);

#endif /* !DECRYPT_H */