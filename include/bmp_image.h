#ifndef BMP_IMAGE_H
#define BMP_IMAGE_H

typedef unsigned short WORD;
typedef unsigned int DWORD;
typedef int LONG;

#pragma pack(push,1)
typedef struct {
    WORD    bf_type;
    DWORD   bf_size;
    WORD   bf_reserved1;
    WORD   bf_reserved2;
    DWORD   bf_offbits;

    DWORD   bi_size;
    LONG    bi_width;
    LONG    bi_height;
    WORD    bi_planes;
    WORD    bi_bit_count;
    DWORD   bi_compression;
    DWORD   bi_size_image;
    LONG    bi_x_pels_per_meter;
    LONG    bi_y_pels_per_meter;
    DWORD   bi_clr_used;
    DWORD   bi_clr_important;
} BmpImageHeader;
#pragma pack(pop)

typedef struct {
    unsigned char red;
    unsigned char green;
    unsigned char blue;
} Pixel;

typedef struct {
    BmpImageHeader header;
    Pixel   **bitmap;
} BmpImage;

int load_image_from_file(BmpImage *image, char *file_path);

int save_image_to_file(BmpImage *image, char *file_path);

void destroy_image(BmpImage *image);

#endif /* !BMP_IMAGE_H */