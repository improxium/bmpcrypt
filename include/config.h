#ifndef CONFIG_H
#define CONFIG_H

typedef struct {
    char *image_file_path;

    int is_input_text_file_path_presented;
    char *input_file_path;

    int is_output_file_path_presented;
    char *output_file_path;

    int mode;
} Config;

Config *create_config(int *res);

void destroy_config(Config *config);

#endif /* !CONFIG_H */