#include <stdio.h>

#ifndef UTILS_H
#define UTILS_H

#define MAX_FILE_LINE_SIZE 256

void readline(char *str, int count, FILE *source);

int read_all_lines(char *str, int count, FILE *source);

int is_file_exists(char *path);

int is_path_valid(char *path);

#endif /* !UTILS_H */