#ifndef COMMON_H
#define COMMON_H

// Max size of most strings in program.
#define DEFAULT_MAX_STRING_SIZE 256

#endif /* !COMMON_H */