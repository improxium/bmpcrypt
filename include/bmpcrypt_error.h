#ifndef BMPCRYPT_ERROR_H
#define BMPCRYPT_ERROR_H

#define EALLOC 1
#define EOPENF 2
#define ECLOSEF 3
#define EFORMATF 4
#define EBMPF 5
#define EIMAGESIZE 6
#define EFILEWRT 7

void print_error(char* message);

void print_error_by_code(int code);

#endif /* !BMPCRYPT_ERROR_H */
