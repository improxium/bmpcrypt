#ifndef ENCRYPT_H
#define ENCRYPT_H

#include "bmp_image.h"

int encrypt_text_to_image(BmpImage *image, char *text);

#endif /* !ENCRYPT_H */